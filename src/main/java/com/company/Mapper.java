package com.company;

import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

public class Mapper {
    public static void main(String[] args) throws URISyntaxException {
        JSONParser parser = new JSONParser();
        AcordMapper am = new AcordMapper();
        RatingMapper rm = new RatingMapper();
        TransferMapper tm = new TransferMapper();
        //calling respective mappers
        Map<String, String> acordMapping = am.acordConverter(parser);
        Map<String, List<String>> ratingMappings = rm.ratingConverter(parser);
        Map<String, List<String>> transferMappings = tm.ratingConverter(parser);
        storeAllIntoCSV(acordMapping, ratingMappings, transferMappings);
    }

    private static void storeAllIntoCSV(Map<String, String> am, Map<String, List<String>> rm,
                                        Map<String, List<String>> tm) throws URISyntaxException {
        //System.out.println("AM=" + am.size() + "RM=" + rm.size() + "TM=" + tm.size());
        File dir = new File(System.getProperty("user.dir") + "/Mapping_Directory");
        if (!dir.exists()) {
            dir.mkdir();
        }
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(dir.getAbsolutePath() + "/" + "finalOutput.csv");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        String columnNameList = "ACORD,SAPI,RATING,TRANSFER ";
        sb.append(columnNameList + "\n");
        boolean ratingFlag = false;
        boolean transferFlag = false;
        //traverse in acord mapping
        for (Map.Entry<String, String> a : am.entrySet()) {
            //go through list of SAPI values
            String sapi = a.getValue();
            ratingFlag = false;
            //for each SAPI values traverse through Rate
            for (Map.Entry<String, List<String>> b : rm.entrySet()) {
                //if both sapi keys are equal
                if (sapi.equals(b.getKey())) {
                    ratingFlag = true;
                    //traverse through rate
                    for (String rating : b.getValue()) {
                        //TODO: Refactor here to remove duplicates row
                        transferFlag = false;
                        //for each value of rate traverse transfer
                        for (Map.Entry<String, List<String>> c : tm.entrySet()) {
                            //if sapi key of rate == transfer
                            if (b.getKey().equals(c.getKey())) {
                                transferFlag = true;
                                //get each value of transfer
                                for (String transfer : c.getValue()) {
                                    //append to file
                                    sb.append(a.getKey() + "," + sapi + "," + rating + "," + transfer + "\n");
                                }
                            }
                        }
                        //flag to check if no mapping found
                        if (!transferFlag) {
                            sb.append(a.getKey() + "," + sapi + "," + rating + "," + "Not Available" + "\n");
                        }
                    }
                }
            }
            //flag to check if no mapping found
            if (!ratingFlag) {
                sb.append(a.getKey() + "," + sapi + "," + "Not Available" + "," + "Not Available" + "\n");
            }
        }
        pw.write(sb.toString());
        pw.close();
    }



    /*
    private static void storeIntoCSV(Map<String, List<String>> mapping, String fileName) throws URISyntaxException {
        File dir = new File(System.getProperty("user.dir") + "/Mapping_Directory");
        if (!dir.exists()) {
            dir.mkdir();
        }
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(dir.getAbsolutePath() + "/" + fileName + ".csv");
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        StringBuilder sb = new StringBuilder();
        String columnNameList = null;
        if (fileName.contains("ACORD")) {
            columnNameList = "ACORD KEY,SAPI KEY";
        } else if (fileName.contains("RATING")) {
            columnNameList = "SAPI KEY,RATING KEY";
        } else {
            columnNameList = "SAPI KEY,TRANSFER KEY";
        }
        sb.append(columnNameList + "\n");
        for (Map.Entry<String, List<String>> a : mapping.entrySet()) {
            for (String temp : a.getValue()) {
                sb.append(a.getKey() + "," + temp + "\n");
            }
        }
        pw.write(sb.toString());
        pw.close();
    }
     */
}
