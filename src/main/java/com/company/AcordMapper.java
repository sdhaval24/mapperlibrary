package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

public class AcordMapper {
    private Map<String, String> acordMapping;

    public AcordMapper() {
        acordMapping = new HashMap<>();
    }

    public Map<String, String> acordConverter(JSONParser parser) throws URISyntaxException {
        URL url = AcordMapper.class.getClassLoader().getResource("acordFiles");
        if (url == null) {
            System.out.println("Directory Not Found");
        } else {
            File filePath = Paths.get(url.toURI()).toFile();
            File[] listOfFiles = filePath.listFiles();
            //to store source-target
            for (File file : listOfFiles) {
                String fileName = file.getName();
                //removing extension
                fileName = fileName.substring(0, fileName.lastIndexOf('.'));
                try (FileReader reader = new FileReader(file.getAbsolutePath())) {
                    Object obj = parser.parse(reader);
                    if (!fileName.contains("Enums")) {
                        JSONObject xmlToJson = (JSONObject) obj;
                        //storing top node
                        JSONObject topNode = (JSONObject) xmlToJson.get("xmlToJson");
                        JSONArray result = (JSONArray) topNode.get(fileName);
                        for (int i = 0; i < result.size(); i++) {
                            JSONObject objects = (JSONObject) result.get(i);
                            String path = (String) objects.get("source");
                            JSONArray targets = (JSONArray) objects.get("targets");
                            if (targets != null) {
                                for (int j = 0; j < targets.size(); j++) {
                                    JSONObject childNode = (JSONObject) targets.get(j);
                                    String sapiNode = childNode.get("target").toString();
                                    sapiNode = sapiNode.replace("*", "{ref}");
                                    acordMapping.put(path + "/" + childNode.get("source").toString(), sapiNode);
                                }
                            } else {
                                acordMapping.put(objects.get("source").toString(), objects.get("target").toString());
                            }
                        }
                    } else {
                        JSONObject enumJson = (JSONObject) obj;
                        Iterator<String> enumKeys = enumJson.keySet().iterator();
                        while (enumKeys.hasNext()) {
                            String enumKey = enumKeys.next();
                            JSONObject node = (JSONObject) enumJson.get(enumKey);
                            enumKey = enumKey.replace("*", "{ref}");
                            Iterator<String> nodeKeys = node.keySet().iterator();
                            while (nodeKeys.hasNext()) {
                                String nodeKey = nodeKeys.next();
                                String nodeValue = (String) node.get(nodeKey);
                                acordMapping.put(enumKey + "/" + nodeKey, nodeValue.toString());
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    System.out.println(e);
                } catch (IOException e) {
                    System.out.println(e);
                } catch (ParseException e) {
                    System.out.println(e);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return acordMapping;
    }

}
