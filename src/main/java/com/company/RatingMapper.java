package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

public class RatingMapper {

    private Map<String, List<String>> ratingMapping;

    public RatingMapper() {
        ratingMapping = new HashMap<>();
    }

    public Map<String, List<String>> ratingConverter(JSONParser parser) throws URISyntaxException {
        URL url = RatingMapper.class.getClassLoader().getResource("ratingFiles/");
        if (url == null) {
            System.out.println("Directory not Found");
        } else {
            File filePath = Paths.get(url.toURI()).toFile();
            File[] listOfFiles = filePath.listFiles();
            for (File file : listOfFiles) {
                String fileName = file.getName();
                try (FileReader reader = new FileReader(file.getAbsolutePath())) {
                    Object obj = parser.parse(reader);
                    if (obj instanceof JSONArray) {
                        JSONArray result = (JSONArray) obj;
                        for (int i = 0; i < result.size(); i++) {
                            JSONObject objects = (JSONObject) result.get(i);
                            String sapiPath = (String) objects.get("source");
                            sapiPath = sapiPath.replace("*", "{ref}");
                            String ratingPath = (String) objects.get("target");
                            if (!ratingMapping.containsKey(sapiPath)) {
                                ratingMapping.put(sapiPath, new ArrayList<String>());
                            }
                            ratingMapping.get(sapiPath).add(ratingPath);
                        }
                    } else {
                        JSONObject enumJson = (JSONObject) obj;
                        Iterator<String> enumKeys = enumJson.keySet().iterator();
                        while (enumKeys.hasNext()) {
                            String enumKey = enumKeys.next();
                            JSONObject node = (JSONObject) enumJson.get(enumKey);
                            enumKey = enumKey.replace("*", "{ref}");
                            Iterator<String> nodeKeys = node.keySet().iterator();
                            while (nodeKeys.hasNext()) {
                                String nodeKey = nodeKeys.next();
                                String nodeValue = String.valueOf(node.get(nodeKey));
                                /*
                                if (!ratingMapping.containsKey(enumKey + "/" + nodeKey)) {
                                    ratingMapping.put(enumKey + "/" + nodeKey, new ArrayList<>());
                                }
                                ratingMapping.get(enumKey + "/" + nodeKey).add(nodeValue);
                                 */
                                if (!ratingMapping.containsKey(nodeKey)) {
                                    ratingMapping.put(nodeKey, new ArrayList<>());
                                }
                                ratingMapping.get(nodeKey).add(nodeValue.toString());
                            }
                        }
                    }

                } catch (FileNotFoundException e) {
                    System.out.println(e);
                } catch (IOException e) {
                    System.out.println(e);
                } catch (ParseException e) {
                    System.out.println(e);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return ratingMapping;
    }
}