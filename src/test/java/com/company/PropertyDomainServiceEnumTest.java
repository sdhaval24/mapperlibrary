package com.company;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class contains a test to assert E1P property enums with respect to Domain Service Property enums
 */

public class PropertyDomainServiceEnumTest {

    static Logger log = Logger.getLogger(PropertyDomainServiceEnumTest.class.getName());

    private ObjectMapper objectMapper;
    private String domainEnumsPath;
    private String e1pEnumsPath;

    @Before
    public void setUp() throws Exception {
        objectMapper = new ObjectMapper();
        domainEnumsPath = "src/test/resources/DomainPropertyEnums.json";
        e1pEnumsPath = "src/test/resources/E1pPropertyEnums.json";
    }

    @Test
    public void validateEnums() throws IOException {
        Map<String, HashMap<String, String>> domainMapping = objectMapper.readValue(new File(domainEnumsPath), HashMap.class);
        Map<String, HashMap<String, String>> e1pMapping = objectMapper.readValue(new File(e1pEnumsPath), HashMap.class);

        for (String s : domainMapping.keySet()) {
            if(e1pMapping.containsKey(s)){
                Map<String,String> map1 = domainMapping.get(s);
                Map<String,String> map2 = e1pMapping.get(s);
                Assert.assertTrue("mapping missing",map1.size() == map2.size());
                Assert.assertTrue("wrong mapping",map1.equals(map2));
            }
        }
    }
}