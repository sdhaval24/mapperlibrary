package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

public class TransferMapper {
    private Map<String, List<String>> transferMapping;

    public TransferMapper() {
        transferMapping = new HashMap<>();
    }

    public Map<String, List<String>> ratingConverter(JSONParser parser) throws URISyntaxException {
        URL url = TransferMapper.class.getClassLoader().getResource("transferFiles/");
        if (url == null) {
            System.out.println("Directory not Found");
        } else {
            File filePath = Paths.get(url.toURI()).toFile();
            File[] listOfFiles = filePath.listFiles();
            for (File file : listOfFiles) {
                String fileName = file.getName();
                try (FileReader reader = new FileReader(file.getAbsolutePath())) {
                    Object obj = parser.parse(reader);
                    if (!fileName.contains("Enums")) {
                        if (obj instanceof JSONObject) {
                            JSONObject topNode = (JSONObject) obj;
                            if (topNode.containsKey("jsonToJson")) {
                                JSONObject request = (JSONObject) topNode.get("jsonToJson");
                                JSONArray result = (JSONArray) request.get("request");
                                for (int i = 0; i < result.size(); i++) {
                                    JSONObject objects = (JSONObject) result.get(i);
                                    String source = (String) objects.get("source");
                                    source = source.replace("*", "{ref}");
                                    String target = (String) objects.get("target");
                                    if (!transferMapping.containsKey(source)) {
                                        transferMapping.put(source, new ArrayList<String>());
                                    }
                                    transferMapping.get(source).add(target);
                                }
                            }
                        } else if (obj instanceof JSONArray) {
                            JSONArray result = (JSONArray) obj;
                            for (int i = 0; i < result.size(); i++) {
                                JSONObject objects = (JSONObject) result.get(i);
                                String sapiPath = (String) objects.get("source");
                                sapiPath = sapiPath.replace("*", "{ref}");
                                String ratingPath = (String) objects.get("target");
                                if (!transferMapping.containsKey(sapiPath)) {
                                    transferMapping.put(sapiPath, new ArrayList<>());
                                }
                                transferMapping.get(sapiPath).add(ratingPath);
                            }
                        } else {
                            JSONObject enumJson = (JSONObject) obj;
                            Iterator<String> enumKeys = enumJson.keySet().iterator();
                            while (enumKeys.hasNext()) {
                                String enumKey = enumKeys.next();
                                JSONObject node = (JSONObject) enumJson.get(enumKey);
                                //replacing here due to above reference
                                enumKey = enumKey.replace("*", "{ref}");
                                Iterator<String> nodeKeys = node.keySet().iterator();
                                while (nodeKeys.hasNext()) {
                                    String nodeKey = nodeKeys.next();
                                    String nodeValue = String.valueOf(node.get(nodeKey));
                                    //replacing here due to above reference
                                    nodeKey = nodeKey.replace("*", "{ref");
                                    if (!transferMapping.containsKey(enumKey + "/" + nodeKey)) {
                                        transferMapping.put(enumKey + "/" + nodeKey, new ArrayList<>());
                                    }
                                    transferMapping.get(enumKey + "/" + nodeKey).add(nodeValue);
                                }
                            }
                        }
                    } else {
                        JSONObject enumJson = (JSONObject) obj;
                        Iterator<String> enumKeys = enumJson.keySet().iterator();
                        while (enumKeys.hasNext()) {
                            String enumKey = enumKeys.next();
                            JSONObject node = (JSONObject) enumJson.get(enumKey);
                            enumKey = enumKey.replace("*", "{ref}");
                            Iterator<String> nodeKeys = node.keySet().iterator();
                            while (nodeKeys.hasNext()) {
                                String nodeKey = nodeKeys.next();
                                String nodeValue = String.valueOf(node.get(nodeKey));
                                /*
                                if (!transferMapping.containsKey(enumKey + "/" + nodeKey)) {
                                    transferMapping.put(enumKey + "/" + nodeKey, new ArrayList<>());
                                }
                                transferMapping.get(enumKey + "/" + nodeKey).add(nodeValue);
                                 */
                                if (!transferMapping.containsKey(nodeKey)) {
                                    transferMapping.put(nodeKey, new ArrayList<>());
                                }
                                transferMapping.get(nodeKey).add(nodeValue.toString());
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    System.out.println(e);
                } catch (IOException e) {
                    System.out.println(e);
                } catch (ParseException e) {
                    System.out.println(e);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return transferMapping;
    }
}
